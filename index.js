'use strict'

var nodeFetch = require('node-fetch')

function tinyStackexchangeSdk() {
}

var apiUrl = 'https://api.stackexchange.com/2.2'

var fetchFn = function (url) {
    try {
        var _fetchFn = nodeFetch
        try {
            if (fetch) {//TODO: find correct way to check if defined
                _fetchFn = fetch
            }
        } catch (err) {
            _fetchFn = nodeFetch
        }
        return _fetchFn(url)
    } catch (err) {
        return Promise.reject(err)
    }
}

var getQuestionInfo = function (questionId, site) {
    try {
        if (!questionId)
            throw new Error('questionId is undefined or null')
        if (!site)
            throw new Error('site is undefined or null')

        var infoUrl = `${this.apiUrl}/questions/${questionId}?site=${site}&page=1&pagesize=1`
        return fetchFn(infoUrl)
            .then(res => res.json())
            .then(questionData => {
                if (questionData.items.length !== 1) {
                    return Promise.reject(new Error('Unable to get question by url'))
                }

                questionData = questionData.items[0]

                return questionData
            })

    } catch (err) {
        return Promise.reject(err)
    }
}

var tryGetResolver = function (questionId, site) {
    return this.getQuestionInfo(questionId, site)
        .then(questionData => {
            try {
                if (questionData.is_answered && questionData.accepted_answer_id) {
                    var answerInfoUrl = `${this.apiUrl}/answers/${questionData.accepted_answer_id}?site=${site}&page=1&pagesize=1`
                    return fetchFn(answerInfoUrl)
                        .then(res => res.json())
                        .then(answerData => {
                            if (!answerData.items || answerData.items.length < 1) {
                                throw new Error('answer data is empty')
                            }
                            return {
                                resolved: true,
                                resolver: (answerData.items.length == 1 ? answerData.items[0].owner.user_id : null)
                            }
                        })
                } else if (questionData.is_answered && !questionData.accepted_answer_id) {
                    return {
                        resolved: true,
                        resolver: null
                    }
                } else {
                    return null
                }
            } catch (err) {
                return Promise.reject(err)
            }
        })
}

var fetchAllUsers = function (apiUrl, ids, site, page, resultArray) {
    var usersUrl = `${apiUrl}/users/${ids}?site=${site}&page=${page}&pagesize=100`
    return fetchFn(usersUrl)
        .then(res => res.json())
        .then(function (res) {
            resultArray = resultArray.concat(res.items ? res.items : [])
            if (res.has_more)
                return fetchAllUsers(ids, site, page + 1, resultArray)
            else {
                return resultArray;
            }
        });
}

var getUsersByIds = function (userIds, site) {
    return fetchAllUsers(this.apiUrl, userIds, site, 1, [])
}

var getUserProfile = function (oAuthToken, site, key) {
    try {
        if (!oAuthToken)
            throw new Error('oAuthToken is undefined or null')

        var profileUrl = `${this.apiUrl}/me?site=${site}&access_token=${oAuthToken}&key=${key}`
        return fetchFn(profileUrl)
            .then(res => res.json())
            .then(profileData => {
                return profileData
            })
    } catch (err) {
        return Promise.reject(err)
    }
}

tinyStackexchangeSdk.prototype = {
    apiUrl: apiUrl,
    getQuestionInfo: getQuestionInfo,
    tryGetResolver: tryGetResolver,
    getUsersByIds: getUsersByIds,
    getUserProfile: getUserProfile
}

module.exports = new tinyStackexchangeSdk()